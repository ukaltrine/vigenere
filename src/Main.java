import java.util.Scanner;

public class Main {

    private static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter a plaintext: ");
        String plainText = sc.nextLine().toLowerCase().replaceAll("\\s", "");

        System.out.println("Enter key: ");
        String key = sc.next().toLowerCase();

        String ciphertext = encrypt(plainText, key);
        System.out.println("\nEncrypted text: " + ciphertext);

        System.out.println("Decrypted text: " + decrypt(ciphertext, key));
    }

    private static String encrypt(String plainText, String key) {
        StringBuilder str = new StringBuilder();

        for (int i = 0, j = 0; i < plainText.length(); i++, j++) {

            if (j == key.length()) {
                j = 0;
            }

            int charPosition = ALPHABET.indexOf(plainText.charAt(i));
            int keyCharPosition = ALPHABET.indexOf(key.charAt(j));
            int position = (charPosition + keyCharPosition) % ALPHABET.length();

            str.append(ALPHABET.charAt(position));
        }
        return str.toString();
    }

    private static String decrypt(String ciphertext, String key) {
        StringBuilder str = new StringBuilder();

        for (int i = 0, j = 0; i < ciphertext.length(); i++, j++) {

            if (j == key.length()) {
                j = 0;
            }

            int charPosition = ALPHABET.indexOf(ciphertext.charAt(i));
            int keyCharPosition = ALPHABET.indexOf(key.charAt(j));
            int position = (charPosition - keyCharPosition + ALPHABET.length()) % ALPHABET.length();

            str.append(ALPHABET.charAt(position));
        }

        return str.toString();
    }
}
